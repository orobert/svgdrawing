﻿using CsPotrace;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvgDrawing
{
    public class SvgShape
    {
        private ArrayList mShape;

        public SvgShape(ArrayList shape, Color color)
        {
            mShape = shape;
            Color = color;
        }

        internal ArrayList InternalShape { get { return mShape; } }

        public Color Color { get; }

        public RectangleF GetBorder()
        {
            return getBorder(mShape);
        }

        public void Offset(float x, float y)
        {
            offsetCurveArray(ref mShape, new PointF(x, y));
        }

        public static SvgShape FromRectangle(RectangleF rect, Color color)
        {
            ArrayList shapes = new ArrayList();

            Potrace.dPoint p1 = new Potrace.dPoint(rect.Left, rect.Top);
            Potrace.dPoint p2 = new Potrace.dPoint(rect.Right, rect.Top);
            Potrace.dPoint p3 = new Potrace.dPoint(rect.Right, rect.Bottom);
            Potrace.dPoint p4 = new Potrace.dPoint(rect.Left, rect.Bottom);

            Potrace.Curve[] curves = new Potrace.Curve[4];

            curves[0] = new Potrace.Curve(Potrace.CurveKind.Line, p1, p1, p2, p2);
            curves[1] = new Potrace.Curve(Potrace.CurveKind.Line, p2, p2, p3, p3);
            curves[2] = new Potrace.Curve(Potrace.CurveKind.Line, p3, p3, p4, p4);
            curves[3] = new Potrace.Curve(Potrace.CurveKind.Line, p4, p4, p1, p1);

            ArrayList shape = new ArrayList();
            shape.Add(curves);

            shapes.Add(shape);

            return new SvgShape(shapes, color);
        }

        public static SvgShape FromPoints(List<PointF> points, Color color)
        {
            ArrayList shapes = new ArrayList();

            Potrace.Curve[] curves = new Potrace.Curve[points.Count];

            for (int i = 1; i < points.Count; i++)
            {
                Potrace.dPoint p1 = new Potrace.dPoint(points[i - 1].X, points[i - 1].Y);
                Potrace.dPoint p2 = new Potrace.dPoint(points[i].X, points[i].Y);

                curves[i - 1] = new Potrace.Curve(Potrace.CurveKind.Line, p1, p1, p2, p2);
            }
            {
                Potrace.dPoint p1 = new Potrace.dPoint(points[points.Count - 1].X, points[points.Count - 1].Y);
                Potrace.dPoint p2 = new Potrace.dPoint(points[0].X, points[0].Y);

                curves[points.Count - 1] = new Potrace.Curve(Potrace.CurveKind.Line, p1, p1, p2, p2);
            }

            ArrayList shape = new ArrayList();
            shape.Add(curves);

            shapes.Add(shape);

            return new SvgShape(shapes, color);
        }

        public static SvgShape FromText(string text, Font font, float h, Color color)
        {
            Bitmap dummy = new Bitmap(100, 100);

            SizeF stringSize = Graphics.FromImage(dummy).MeasureString(text, font);
            dummy.Dispose();

            Bitmap image = new Bitmap((int)(stringSize.Width * 1.1), (int)(stringSize.Height * 1.1));
            Graphics gr = Graphics.FromImage(image);
            gr.Clear(Color.White);

            Brush br = new SolidBrush(Color.Black);
            gr.DrawString(text, font, br, new PointF(0, 0));

            SvgShape shape;
            {
                bool[,] matrix = Potrace.BitMapToBinary(image, 130);
                ArrayList shapes = new ArrayList();
                Potrace.potrace_trace(matrix, shapes);

                RectangleF rect = getBorder(shapes);
                float scale = h / rect.Height;

                scaleCurveArray(ref shapes, scale);

                shape = new SvgShape(shapes, color);
            }

            image.Dispose();
            return shape;
        }

        private static RectangleF getBorder(ArrayList listOfCurveArray)
        {
            double minX = double.MaxValue;
            double minY = double.MaxValue;
            double maxX = double.MinValue;
            double maxY = double.MinValue;

            for (int k = 0; k < listOfCurveArray.Count; k++)
            {
                ArrayList path = (ArrayList)listOfCurveArray[k];

                for (int i = 0; i < path.Count; i++)
                {
                    Potrace.Curve[] curves = (Potrace.Curve[])path[i];

                    for (int j = 0; j < curves.Length; j++)
                    {
                        minX = Math.Min(minX, curves[j].A.x);
                        minX = Math.Min(minX, curves[j].B.x);

                        minY = Math.Min(minY, curves[j].A.y);
                        minY = Math.Min(minY, curves[j].B.y);

                        maxX = Math.Max(maxX, curves[j].A.x);
                        maxX = Math.Max(maxX, curves[j].B.x);

                        maxY = Math.Max(maxY, curves[j].A.y);
                        maxY = Math.Max(maxY, curves[j].B.y);
                    }
                }
            }

            return new RectangleF((float)minX, (float)minY, (float)(maxX - minX), (float)(maxY - minY));
        }


        private static void scaleCurveArray(ref ArrayList listOfCurveArray, float scale)
        {
            scaleCurveArray(ref listOfCurveArray, scale, scale);
        }

        private static void scaleCurveArray(ref ArrayList listOfCurveArray, float scaleX, float scaleY)
        {
            for (int k = 0; k < listOfCurveArray.Count; k++)
            {
                ArrayList path = (ArrayList)listOfCurveArray[k];

                for (int i = 0; i < path.Count; i++)
                {
                    Potrace.Curve[] curves = (Potrace.Curve[])path[i];

                    for (int j = 0; j < curves.Length; j++)
                    {
                        scaleCurvePoint(ref curves[j].A, scaleX, scaleY);
                        scaleCurvePoint(ref curves[j].B, scaleX, scaleY);
                        scaleCurvePoint(ref curves[j].ControlPointA, scaleX, scaleY);
                        scaleCurvePoint(ref curves[j].ControlPointB, scaleX, scaleY);
                    }
                }
            }
        }

        private static void scaleCurvePoint(ref Potrace.dPoint point, float scaleX, float scaleY)
        {
            point.x *= scaleX;
            point.y *= scaleY;
        }

        private static void offsetCurveArray(ref ArrayList listOfCurveArray, PointF offsetPoint)
        {
            for (int k = 0; k < listOfCurveArray.Count; k++)
            {
                ArrayList path = (ArrayList)listOfCurveArray[k];

                for (int i = 0; i < path.Count; i++)
                {
                    Potrace.Curve[] curves = (Potrace.Curve[])path[i];

                    for (int j = 0; j < curves.Length; j++)
                    {
                        offsetCurvePoint(ref curves[j].A, offsetPoint);
                        offsetCurvePoint(ref curves[j].B, offsetPoint);
                        offsetCurvePoint(ref curves[j].ControlPointA, offsetPoint);
                        offsetCurvePoint(ref curves[j].ControlPointB, offsetPoint);
                    }
                }
            }
        }

        private static void offsetCurvePoint(ref Potrace.dPoint point, PointF offsetPoint)
        {
            point.x += offsetPoint.X;
            point.y += offsetPoint.Y;
        }
    }
}
