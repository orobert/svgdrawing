﻿using CsPotrace;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SvgDrawing
{
    public class SvgDocument
    {
        private List<SvgShape> mCutShapes = new List<SvgShape>();
        private List<SvgShape> mWriteShapes = new List<SvgShape>();

        public void AddCutShape(SvgShape shape)
        {
            mCutShapes.Add(shape);
        }
        public void AddCutShape(List<SvgShape> shapes)
        {
            mCutShapes.AddRange(shapes);
        }

        public void AddWriteShape(SvgShape shape)
        {
            mWriteShapes.Add(shape);
        }
        public void AddWriteShape(List<SvgShape> shapes)
        {
            mWriteShapes.AddRange(shapes);
        }


        public void Save(string path)
        {
            string cutSvg = GetSvgShapes(mCutShapes, true);
            string writeSvg = GetSvgShapes(mWriteShapes, false);

            //<svg version="1.0" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" width="304.79999mm" height="304.79999mm" viewBox="0 0 304.80001 304.80003">
            //</svg>

            StreamWriter FS = new StreamWriter(path);
            FS.WriteLine("<svg version=\"1.0\" xmlns=\"http://www.w3.org/2000/svg\" preserveAspectRatio=\"xMidYMid meet\" width=\"304.79999mm\" height=\"304.79999mm\" viewBox=\"0 0 304.80001 304.80003\">");
            if (cutSvg.Length > 0)
            {
                FS.WriteLine("<g transform=\"translate(0,0)\">");
                FS.WriteLine(cutSvg);
                FS.WriteLine("</g>");
            }
            if (writeSvg.Length > 0)
            {
                FS.WriteLine("<g transform=\"translate(0,0)\">");
                FS.WriteLine(writeSvg);
                FS.WriteLine("</g>");
            }
            FS.WriteLine("</svg>");
            FS.Close();
        }

        private string GetSvgShapes(List<SvgShape> shapes, bool cut)
        {
            List<ArrayList> list = new List<ArrayList>();

            for (int i = 0; i < shapes.Count; i++)
            {
                list.Add(shapes[i].InternalShape);
            }

            string xml = Potrace.Export2SVG(list, 304.80001f, 304.80003f);

            int pos = xml.IndexOf("<svg");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml.Substring(pos));

            XmlNode svgNode = xmlDoc.ChildNodes[0];

            for (int i = 0; i < shapes.Count; i++)
            {
                SvgShape shape = shapes[i];

                XmlNode groupNode = svgNode.ChildNodes[i];
                {
                    XmlAttribute styleAttrib = xmlDoc.CreateAttribute("style");
                    if (cut)
                        styleAttrib.Value = string.Format("fill:#{0:x6};stroke:none", (shape.Color.ToArgb() & 0xffffff));
                    else
                        styleAttrib.Value = string.Format("fill:none;stroke:#{0:x6};stroke-width:0.4", (shape.Color.ToArgb() & 0xffffff));
                    groupNode.Attributes.Append(styleAttrib);
                }
                {
                    XmlAttribute styleAttrib = xmlDoc.CreateAttribute("transform");
                    styleAttrib.Value = "translate(0,0)";
                    groupNode.Attributes.Append(styleAttrib);
                }
            }

            return xmlDoc.ChildNodes[0].InnerXml;
        }
    }
}
